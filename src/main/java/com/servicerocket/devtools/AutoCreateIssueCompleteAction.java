package com.servicerocket.devtools;

import com.atlassian.bamboo.build.CustomBuildProcessorServer;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.fileserver.SystemDirectory;
import com.atlassian.bamboo.comment.CommentManager;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationManager;
import com.atlassian.event.api.EventPublisher;


import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityLinkService;


import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
//import  com.atlassian.sal.api.transaction.TransactionTemplate;
//import  com.atlassian.sal.api.transaction.TransactionCallback;


public class AutoCreateIssueCompleteAction implements CustomBuildProcessorServer {
    private static final Logger log = Logger.getLogger(AutoCreateIssueCompleteAction.class);
    private BuildLoggerManager buildLoggerManager;
    private BuildContext buildContext;
    private EntityLinkService entityLinkService;
    private EntityLink entityLink;
    private ProjectManager projectManager;
    private CommentManager commentManager;
    private ResultsSummaryManager resultsSummaryManager;
    private AdministrationConfigurationManager administrationConfigurationManager;
    private EventPublisher eventPublisher;
    private String newIssueKey = null;

    //Adding constructor

    public AutoCreateIssueCompleteAction(){


    }

    public void setEntityLinkService(@NotNull final EntityLinkService entityLinkService)
    {
        this.entityLinkService = entityLinkService;
    }

    public void setEntityLink(@NotNull final EntityLink entityLink)
    {
        this.entityLink = entityLink;
    }

    public void setProjectManager(@NotNull final ProjectManager projectManager)
    {
        this.projectManager = projectManager;
    }

    public void setCommentManager(@NotNull final CommentManager commentManager)
    {
        this.commentManager = commentManager;
    }

    public void setResultsSummaryManager(@NotNull final ResultsSummaryManager resultsSummaryManager)
    {
        this.resultsSummaryManager = resultsSummaryManager;
    }


    public void setAdministrationConfigurationManager(@NotNull final AdministrationConfigurationManager administrationConfigurationManager)
    {
        this.administrationConfigurationManager = administrationConfigurationManager;
    }

    public void setEventPublisher(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    public void init(@NotNull final BuildContext context ) {
        this.buildContext = context;
    }

    @NotNull
    @Override
    public BuildContext call() throws InterruptedException,Exception {


        log.info("Sultan`s call method is about to start plugin execution");
        final BuildState buildState = getBuildState();
        // Instantiating the resultssummary for use to check additional conditions; rerun and previous builds
        ResultsSummary resultsSummary = resultsSummaryManager.getResultsSummary(buildContext.getPlanResultKey());
        if(resultsSummary == null)
        {
            log.error("Results summary can not be null");
        }


        if (buildState.equals(BuildState.SUCCESS))
        {
            log.info("build was successful, autocreateissue plugin will abort here");
            addBuildLogEntry("build was successful, autocreate issue will abort");

        } //if for build status

        else if (buildState.equals(BuildState.FAILED)&& !buildContext.isCustomBuild()&& buildContext.getTriggerReason().toString()!="Rerun build")
        {
            addBuildLogEntry("build failed, running autocreateissue");
            log.info("build failed, Bamboo will attempt to create issue");
            //ApplicationLink
            entityLink = entityLinkService.getPrimaryEntityLink(projectManager.getProjectByName(buildContext.getProjectName()), JiraProjectEntityType.class);
            if (entityLink == null)
            {
                log.error("No Link to a JIRA for this Bamboo Project configured");
            }
            else if(entityLink != null)
            {
                log.info("Project link to JIRA exist for this Bamboo project");
                String key = entityLink.getKey();
                String appLink = entityLink.getApplicationLink().toString();
                String description = "Description is empty for now";
                String url = entityLink.getApplicationLink().getRpcUrl().toString();
                String summary = "Issue created from failed build of "+buildContext.getBuildResultKey();
                String reporter = "admin";
                //String newIssueKey = null;
                // Preparing the JSON fields
                JSONObject issueCreatorJson = new JSONObject();

                //issueCreatorJson.put("key", key);
                String data = "{\"fields\":{\"project\":{\"key\":\""+key+"\"},\"description\":\""+description+"\",\"summary\":\""+summary+"\",\"issuetype\":{\"name\":\"Bug\"}}}";
                issueCreatorJson.put("summary", summary);
                issueCreatorJson.put("description", description);
                issueCreatorJson.put("issuetype", "{"+ "name:"+"Bug}");
                issueCreatorJson.put("reporter", reporter);

                ApplicationLinkRequestFactory requestFactory = entityLink.getApplicationLink().createAuthenticatedRequestFactory();
                try
                {
                    ApplicationLinkRequest requestIssueCreate = requestFactory.createRequest(MethodType.POST, url+"/rest/api/latest/issue/");
                    requestIssueCreate.setRequestContentType("application/json");
                    requestIssueCreate.setRequestBody(data);

                    String responseIssueCreate = requestIssueCreate.execute(new ApplicationLinkResponseHandler<String>()
                    {
                        public String credentialsRequired(final Response response) throws ResponseException
                        {
                            return response.getResponseBodyAsString();
                        }

                        public String handle(final Response response) throws ResponseException
                        {
                            return response.getResponseBodyAsString();
                        }
                    });
                    JSONObject issueObj = new JSONObject(responseIssueCreate);
                    newIssueKey = issueObj.getString("key");

                }
                catch (CredentialsRequiredException e)
                {

                }

                try
                {
                    ApplicationLinkRequest requestIssueAttachment = requestFactory.createRequest(MethodType.POST, url+"/rest/api/latest/issue/"+newIssueKey+"/attachments");

                    File fileDirectory = SystemDirectory.getBuildLogsDirectory(buildContext.getPlanKey());
                    String buildLogText =fileDirectory.getPath()+"\\"+buildContext.getBuildResultKey()+".log";
                    File buildLog = new File(buildLogText);

                    requestIssueAttachment.addHeader("X-Atlassian-Token", "nocheck");
                    requestIssueAttachment.setRequestContentType("text/plain");
                    RequestFilePart requestFilePart = new RequestFilePart("text/plain", buildContext.getBuildResultKey()+".log", buildLog, "file");
                    List<RequestFilePart> fileParts = new ArrayList<RequestFilePart>();
                    fileParts.add(requestFilePart);
                    requestIssueAttachment.setFiles(fileParts);

                    String responseIssueAttached = requestIssueAttachment.execute();
                    //JSONObject attachObj = new JSONObject(responseIssueAttached);
                    //String attachmentAck = attachObj.getString("self");

                    log.info("Attachment uploaded successfully to issue key");
                      /**
                    transactionTemplate.execute(new TransactionCallback<Object>()
                    {
                        @Override
                        public Object doInTransaction()
                        {
                            BuildCommentAndLinkIssue();
                            //do what ever it is you need to do
                            return null;
                        }
                    });

                     **/


                }

                catch (CredentialsRequiredException e)
                {

                }
              //Getting the Bamboo base URL
                AdministrationConfiguration administrationConfiguration = administrationConfigurationManager.getAdministrationConfiguration();
                String bambooURL = administrationConfiguration.getBaseUrl();

               // Organizing the buildkey to be used in the REST APi URL. It will be something like  TESTA-TSTA-40
                String restBuildKey = buildContext.getPlanResultKey().toString().replaceFirst("-"+resultsSummary.getPlan().getBuildKey(),"");


                try
                {
                    ApplicationLinkRequest requestBuildComment = requestFactory.createRequest(MethodType.POST, bambooURL+"/rest/api/latest/result/"+restBuildKey+"/comment");

                    String comment = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                            "<comment>\n" +
                            "    <content>New Issue created with the key " +newIssueKey+"</content>\n" +
                            "</comment>";

                    requestBuildComment.addHeader("X-Atlassian-Token", "nocheck");
                    requestBuildComment.setRequestContentType("application/xml");
                    requestBuildComment.setRequestBody(comment);


                    String responseBuildCommented = requestBuildComment.execute();

                }

                catch (CredentialsRequiredException e)
                {

                }




            }//else if for null check on entitylinks
        }//else if for buildstate == fail

        return buildContext;
    }


    //writing out some private methods to be used within the class
    private void addBuildLogEntry(String entry) {
        if(buildLoggerManager != null)
        {
            buildLoggerManager.getBuildLogger(buildContext.getPlanResultKey()).addBuildLogEntry(entry);
        }
    }

    private BuildState getBuildState()
    {
        BuildState buildState = buildContext.getBuildResult().getBuildState();
        if (buildState.equals(BuildState.UNKNOWN))
        {

            if (0 < buildContext.getBuildResult().getBuildReturnCode())
            {
                buildState = BuildState.FAILED;
            } else
            {
                buildState = BuildState.SUCCESS;
            }
        }

        return buildState;
    }// end of the getBuildState method

}//end of class
